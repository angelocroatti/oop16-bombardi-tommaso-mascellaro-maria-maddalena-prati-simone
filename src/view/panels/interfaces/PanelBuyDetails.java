package view.panels.interfaces;

import model.admin.products.BuyObject;
/**
 * 
 * panel by details interface.
 *
 */
public interface PanelBuyDetails extends Panel, PanelDetails {
    /**
     * 
     * @param ob
     *          object to buy
     */
    void setObject(BuyObject ob);
}
