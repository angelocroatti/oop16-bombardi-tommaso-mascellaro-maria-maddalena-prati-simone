package view.panels.interfaces;

import java.util.Map;

import javax.swing.JButton;
import model.admin.products.RentObject;
/**
 * 
 *panel rent interface.
 *
 */
public interface PanelRentStorage extends Panel {
    /**
     * 
     * @return button for panel cart
     */
    JButton getBtnCart();
    /**
     * 
     * @return map with buttons of the items to rent
     */
    Map<JButton, RentObject> getBtnObj();
}
