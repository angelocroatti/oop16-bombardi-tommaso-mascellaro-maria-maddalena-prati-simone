package view.panels.interfaces;

import javax.swing.JButton;
import javax.swing.JPanel;
/**
 * 
 * interface for panels.
 *
 */
public interface Panel {
    /**
     * 
     * @return panel 
     */
    JPanel getPanel();
    /**
     * 
     * @return button for previous panel
     */
    JButton getBtnPrev();
}
