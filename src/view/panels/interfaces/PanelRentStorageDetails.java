package view.panels.interfaces;

import model.admin.products.RentObject;
/**
 * 
 * panel rent details interface.
 *
 */
public interface PanelRentStorageDetails  extends PanelDetails {
    /**
     * 
     * @param ob
     *          object
     */
    void setObject(RentObject ob);

}
