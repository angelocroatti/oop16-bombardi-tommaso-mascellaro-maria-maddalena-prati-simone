package view.panels.interfaces;

import javax.swing.JButton;
import javax.swing.JTextField;
/**
 * 
 * panel login administrator interface.
 *
 */
public interface PanelLoginAdmin extends Panel {
    /**
     * 
     * @return user name
     */
    JTextField getUsername();
    /**
     * 
     * @return password
     */
    JTextField getPassword();
    /**
     * 
     * @return button login
     */
    JButton getBtnLogin();
}
