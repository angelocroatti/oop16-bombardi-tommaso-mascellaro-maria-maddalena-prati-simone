package view.panels.interfaces;

import java.util.Map;

import javax.swing.JButton;

import model.admin.products.BuyObject;
/**
 * 
 * panel buy interface.
 *
 */
public interface PanelBuy extends Panel {
    /**
     * 
     * @return button for the panel cart
     */
    JButton getBtnCart();
    /**
     * 
     * @return map with buttons of the items to buy
     */
    Map<JButton, BuyObject> getBtnObj();
}
