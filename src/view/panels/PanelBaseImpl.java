package view.panels;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import view.panels.interfaces.PanelBase;

/**
 * 
 * panel base class.
 *
 */
public class PanelBaseImpl extends JPanel implements PanelBase {
    private static final long serialVersionUID = 1L;
    private final JPanel panelWelcome;
    private final JButton btnUser;
    private final JButton btnAdmin;
    private static final int LAB = FrameSize.WIDTH.getValue() / 20;
    private static final int WC = FrameSize.WIDTH.getValue() / 6;
    private static final int HC = (int) (FrameSize.HEIGHT.getValue() / 1.3);
    private static final int WC1 = (int) (FrameSize.WIDTH.getValue() / 1.8);
    private static final int HC1 = FrameSize.HEIGHT.getValue() / 10;
    private static final int WC2 = (int) (FrameSize.WIDTH.getValue() / 3.3);

    private static final int WCT = FrameSize.WIDTH.getValue() / 8;
    private static final int HCT = FrameSize.HEIGHT.getValue() / 20;

    /**
     * panel base constructor.
     */
    public PanelBaseImpl() {
        super();
        //PANNELLO BASE
        final CardLayout cl = new CardLayout();
        this.setLayout(cl);
        //PANNELLO DI INIZIO
        this.panelWelcome = new JPanel();
        //SFONDO
        final ImageIcon img = new ImageIcon(getClass().getResource("/sfondo.jpg"));
        final Image scaledImage = img.getImage().getScaledInstance(FrameSize.WIDTH.getValue(), FrameSize.HEIGHT.getValue(), Image.SCALE_DEFAULT);
        img.setImage(scaledImage);
        final JLabel labelImg = new JLabel(img);
        labelImg.setBounds(0, 0, FrameSize.WIDTH.getValue(), FrameSize.HEIGHT.getValue());
        //TITOLO
        final JLabel benvenuto = new JLabel("Benvenuto in SkiCenter Manager");
        benvenuto.setBounds(WCT, HCT, FrameSize.WIDTH.getValue(), HC1);
        benvenuto.setFont(new Font("Tahoma", Font.PLAIN, LAB));
        benvenuto.setForeground(Color.RED);
        this.btnUser = new JButton("Accedi come utente");
        this.btnUser.setBounds(WC, HC, WC2, HC1);
        this.btnAdmin = new JButton("Accedi come amministratore");
        this.btnAdmin.setBounds(WC1, HC, WC2, HC1);
        labelImg.add(btnAdmin);
        labelImg.add(btnUser);
        labelImg.add(benvenuto);
        this.panelWelcome.add(labelImg);
        this.panelWelcome.setLayout(null);
        this.panelWelcome.setVisible(false);
        this.setVisible(true);
    }

    @Override
    public JPanel getPanelBase() {
        return this;
    }

    @Override
    public JPanel getPanelWelcome() {
        return this.panelWelcome;
    }

    @Override
    public JButton getBtnUserPanel() {
        return this.btnUser;
    }

    @Override
    public JButton getBtnAdminPanel() {
        return this.btnAdmin;
    }
}
