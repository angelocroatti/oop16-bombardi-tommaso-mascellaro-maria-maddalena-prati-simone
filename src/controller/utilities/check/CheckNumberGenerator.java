package controller.utilities.check;

import java.util.Optional;

/**
 * Class created to generate check number functions, designed using strategy pattern.
 */
public final class CheckNumberGenerator implements CheckNumber {

    private final CheckNumber checkNum;

    /**
     * Constructor for check number generator.
     * 
     * @param check
     *              implementation of interface check number that will be used
     */
    public CheckNumberGenerator(final CheckNumber check) {
        super();
        this.checkNum = check;
    }

    @Override
    public Optional<Integer> check(final String num, final int max) {
        return this.checkNum.check(num, max);
    }

}
