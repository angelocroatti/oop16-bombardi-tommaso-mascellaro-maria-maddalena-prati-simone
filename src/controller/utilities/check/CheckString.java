package controller.utilities.check;

import java.util.Optional;

/**
 * Interface for check string algorithm.
 */
public interface CheckString {

    /**
     * Check string.
     * 
     * @param type
     *              enum value which represents the type of check
     * @param str
     *              string that will be checked
     * @param min
     *              integer used to implement the check algorithm, in the implementation
     *              done in the factory it represents the min length of the string
     * @param max
     *              integer used to implement the check algorithm, in the implementation
     *              done in the factory it represents the max length of the string
     * @return an optional
     *              this optional can have different meanings according to the algorithm,
     *              in the implementation done in the factory it is with the string value if 
     *              it is valid and empty otherwise
     *
     */
    Optional<String> check(CheckType type, String str, int min, int max);

    /**
     * Type of the string that will be checked.
     */
    enum CheckType {
        LENGTH,
        NAME,
        STRING;
    }

}
